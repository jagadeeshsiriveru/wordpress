<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'staging' );

/** Database username */
define( 'DB_USER', 'staging' );

/** Database password */
define( 'DB_PASSWORD', 'P@ssw0rd' );

/** Database hostname */
define( 'DB_HOST', 'database-1.cjlbvmjwupw3.us-east-1.rds.amazonaws.com' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '(J5|l{)NFL<( Sw68Xk0|jnm@N>y_k;>1vM,D@a;NV(2?3!2U6GW3g! {bg_*2gR' );
define( 'SECURE_AUTH_KEY',  '8n>wpWM-,v@ 4^2W?G>_;4<^WvbPn [ GU,%gL,9:pGhMPL7re+|nsT`e]7l61_L' );
define( 'LOGGED_IN_KEY',    'vt1KPg7Ri>E/OFBJF1WLz=>zKqNQXD<fP>|H:%^%mz-`kRjF~o)F7hCNp@7n)r]_' );
define( 'NONCE_KEY',        'fJK qxp2}Se^Q9)@Ec_ac>{tuQh){7*IipbvnXW@G^^IBImcYht #pb^ELvIB%55' );
define( 'AUTH_SALT',        'i^ypZO*{f1!)S})/(k>bM=VSojZsX3:SLB*A,4beybhZ$c?6N:>e6KYH(!W9[Mx-' );
define( 'SECURE_AUTH_SALT', 'NL7}A?uyn5B`O(~5@F-w/R?`] XR4aURq](Q*7QIVfi)n/UUG],eL{wITt[b)>v7' );
define( 'LOGGED_IN_SALT',   '?TUVR-:US1;vAAg[]k7:vxq+q=wp^-S4099pD*Au}IS_RC6n`+hUI/TJKu=`XxwD' );
define( 'NONCE_SALT',       '<juU7c YN.~ktt2HN_/D L_h_qiJPIH/s47LiwG3*rA7A!**@Q;gw+nhc<,/rg;|' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
