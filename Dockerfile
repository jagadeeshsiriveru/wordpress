FROM base-image:3
RUN mkdir -p /var/www/wordpress && \ 
 chown -R www-data:www-data /var/www/wordpress && \
touch /var/log/mail.log  && \
 chmod 777 /var/log/mail.log &&  \
 mkdir -p /var/local/cache && \
 chmod 777 /var/local/cache &&  \
 unlink /etc/nginx/sites-enabled/default
####
RUN echo "\ndaemon off;" >> /etc/nginx/nginx.conf
RUN chown -R www-data:www-data /var/lib/nginx
# expose ports
EXPOSE 80
COPY ./wordpress /etc/nginx/sites-available/
RUN rm -rf /etc/nginx/sites-enabled/default
# create symlinks
RUN ln -s /etc/nginx/sites-available/wordpress /etc/nginx/sites-enabled/
######
COPY ./ /var/www/wordpress 
COPY entrypoint.sh /var/www/
RUN chmod +x /var/www/entrypoint.sh
WORKDIR /var/www/wordpress
EXPOSE 443
EXPOSE 80

ENTRYPOINT /var/www/entrypoint.sh
CMD ["nginx"]

